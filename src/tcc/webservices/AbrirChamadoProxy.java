package tcc.webservices;

public class AbrirChamadoProxy implements tcc.webservices.AbrirChamado_PortType {
  private String _endpoint = null;
  private tcc.webservices.AbrirChamado_PortType abrirChamado_PortType = null;
  
  public AbrirChamadoProxy() {
    _initAbrirChamadoProxy();
  }
  
  public AbrirChamadoProxy(String endpoint) {
    _endpoint = endpoint;
    _initAbrirChamadoProxy();
  }
  
  private void _initAbrirChamadoProxy() {
    try {
      abrirChamado_PortType = (new tcc.webservices.AbrirChamado_ServiceLocator()).getAbrirChamadoPort();
      if (abrirChamado_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)abrirChamado_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)abrirChamado_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (abrirChamado_PortType != null)
      ((javax.xml.rpc.Stub)abrirChamado_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public tcc.webservices.AbrirChamado_PortType getAbrirChamado_PortType() {
    if (abrirChamado_PortType == null)
      _initAbrirChamadoProxy();
    return abrirChamado_PortType;
  }
  
  public java.lang.String hello(java.lang.String name) throws java.rmi.RemoteException{
    if (abrirChamado_PortType == null)
      _initAbrirChamadoProxy();
    return abrirChamado_PortType.hello(name);
  }
  
  public void abreChamado(java.lang.String nome, java.lang.String data, java.lang.String hora, java.lang.String telefone, java.lang.String celular, java.lang.String endereco, java.lang.String problema) throws java.rmi.RemoteException{
    if (abrirChamado_PortType == null)
      _initAbrirChamadoProxy();
    abrirChamado_PortType.abreChamado(nome, data, hora, telefone, celular, endereco, problema);
  }
  
  
}