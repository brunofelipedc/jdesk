/**
 * AbrirChamado_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tcc.webservices;

public interface AbrirChamado_PortType extends java.rmi.Remote {
    public java.lang.String hello(java.lang.String name) throws java.rmi.RemoteException;
    public void abreChamado(java.lang.String nome, java.lang.String data, java.lang.String hora, java.lang.String telefone, java.lang.String celular, java.lang.String endereco, java.lang.String problema) throws java.rmi.RemoteException;
}
