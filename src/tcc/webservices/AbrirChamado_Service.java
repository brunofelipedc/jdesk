/**
 * AbrirChamado_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tcc.webservices;

public interface AbrirChamado_Service extends javax.xml.rpc.Service {
    public java.lang.String getAbrirChamadoPortAddress();

    public tcc.webservices.AbrirChamado_PortType getAbrirChamadoPort() throws javax.xml.rpc.ServiceException;

    public tcc.webservices.AbrirChamado_PortType getAbrirChamadoPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
