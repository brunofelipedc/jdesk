/**
 * AbrirChamado_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tcc.webservices;

public class AbrirChamado_ServiceLocator extends org.apache.axis.client.Service implements tcc.webservices.AbrirChamado_Service {

    public AbrirChamado_ServiceLocator() {
    }


    public AbrirChamado_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public AbrirChamado_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for AbrirChamadoPort
    private java.lang.String AbrirChamadoPort_address = "http://localhost:8080/WSJdesk/AbrirChamado";

    public java.lang.String getAbrirChamadoPortAddress() {
        return AbrirChamadoPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String AbrirChamadoPortWSDDServiceName = "AbrirChamadoPort";

    public java.lang.String getAbrirChamadoPortWSDDServiceName() {
        return AbrirChamadoPortWSDDServiceName;
    }

    public void setAbrirChamadoPortWSDDServiceName(java.lang.String name) {
        AbrirChamadoPortWSDDServiceName = name;
    }

    public tcc.webservices.AbrirChamado_PortType getAbrirChamadoPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(AbrirChamadoPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getAbrirChamadoPort(endpoint);
    }

    public tcc.webservices.AbrirChamado_PortType getAbrirChamadoPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            tcc.webservices.AbrirChamadoPortBindingStub _stub = new tcc.webservices.AbrirChamadoPortBindingStub(portAddress, this);
            _stub.setPortName(getAbrirChamadoPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setAbrirChamadoPortEndpointAddress(java.lang.String address) {
        AbrirChamadoPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (tcc.webservices.AbrirChamado_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                tcc.webservices.AbrirChamadoPortBindingStub _stub = new tcc.webservices.AbrirChamadoPortBindingStub(new java.net.URL(AbrirChamadoPort_address), this);
                _stub.setPortName(getAbrirChamadoPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("AbrirChamadoPort".equals(inputPortName)) {
            return getAbrirChamadoPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservices.tcc/", "AbrirChamado");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservices.tcc/", "AbrirChamadoPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("AbrirChamadoPort".equals(portName)) {
            setAbrirChamadoPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
